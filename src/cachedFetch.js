import fetch from 'node-fetch'

export default (url, options) => {

    if(typeof localStorage === 'undefined') return fetch(url, options)

    // const cachedFetch = (url, options) => {
    let expiry = 2 * 60 * 60 // 2 horas default
    if (typeof options === 'number') {
      expiry = options
      options = undefined
    } else if (typeof options === 'object') {
      expiry = options.seconds || expiry
    }
    let cacheKey = url
    let cached = localStorage.getItem(cacheKey)
    let whenCached = localStorage.getItem(cacheKey + ':ts')
    if (cached !== null && whenCached !== null) {
      let age = (Date.now() - whenCached) / 1000
      if (age < expiry) {
        let response = new Response(new Blob([cached]))
        return Promise.resolve(response)
      } else {
        localStorage.removeItem(cacheKey)
        localStorage.removeItem(cacheKey + ':ts')
      }
    }
    return fetch(url, options).then(response => {
      if (response.status === 200) {
        let ct = response.headers.get('Content-Type')
        if (ct && (ct.match(/application\/json/i) || ct.match(/text\//i))) {
          response.clone().text().then(content => {
            localStorage.setItem(cacheKey, content)
            localStorage.setItem(cacheKey + ':ts', Date.now())
          })
        }
      }
      return response
    })
  }
