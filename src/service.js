import fetch from 'node-fetch'
// import cachedFetch from './cachedFetch'

const endpointDetails = 'https://pokeapi.co/api/v2/'
const endpoint = 'https://api.dev.perfivo.com/pokeapi/v0/'

const handle = p =>p.then(res => [res,]).catch(err => Promise.resolve([, err]))

const getAll = async () => await handle(fetch(endpointDetails+'pokemon/').then(r=>r.json()))

const getDetail = async id => await handle(fetch(endpointDetails+'pokemon/'+id).then(r=>r.json()))
// const getDetail = async id => await handle(cachedFetch(endpointDetails+'pokemon/'+id).then(r=>r.json()))

const getRegionTrainers = async () => await handle(fetch(endpoint+'trainers/').then(r=>r.json()))

const getRegionPokemons = async () => await handle(fetch(endpoint+'pokemon/').then(r=>r.json()))

const create = async (type, data) => await handle(fetch(endpoint+type+'/', {
  method: 'POST',
  body: JSON.stringify(data),
  headers:{
    'Content-Type': 'application/json'
  }
}).then(r => r.json()))

const edit = async (type, id, data) => await handle(fetch(`${endpoint}${type}/${id}/`, {
  method: 'PUT',
  body: JSON.stringify(data),
  headers:{
    'Content-Type': 'application/json'
  }
}).then(r => r.json()))

const remove = async (type, id) => await handle(fetch(`${endpoint}${type}/${id}/`, {
  method: 'DETELE'
}).then(r => r.json()))

export {
  edit,
  create,
  remove,
  getAll,
  getDetail,
  getRegionTrainers,
  getRegionPokemons
}
