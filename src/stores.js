import { writable } from 'svelte/store';

export const regionTrainers = writable([]);
export const regionPokemons = writable([]);
// export const pokemons = writable([]);